<!--
NOTE: we use node-exporter to send data to prometheus?  My guess is it may be designed more for clusters, node x and node y both send data to prometheus and subsequently to grafana.
-->

##### 0. Make sure you have the proper tools   

This guide assumes you've already installed and configured Prometheus & Grafana.

##### 1.  Install Node Exporter   

Download latest node exporter package (we're using Linux amd64)

[Node Exporter Releases](https://github.com/prometheus/node_exporter/releases)   

untar the package

~~~sh
#unpack the tarball
~$ tar xvfz node_exporter-1.2.2.linux-amd64.tar.gz
~$ cd node_exporter-1.2.2.linux-amd64/
~$ ./node_exporter
~~~

node_exporter is now listening one port 9100

##### 2. Configure Prometheus to use this running instance of node_exporter

Go to wherever you've installed prometheus, and modify the config file to hit the port that node_exporter exposes.   

~~~yml
#add node_exporters port to the static_configs - targets        v
    static_configs:
        - targets: ["localhost:9090", "localhost:9091", "localhost:9100"]
~~~

+ start or restart prometheus

+ open up your prometheus GUI and make some test queries to the metrics that node_exporter collects!

~~~txt
node_filesystem_avail_bytes{mountpoint="/"}
rate(node_network_receive_bytes_total[1m])
rate(node_cpu_seconds_total{mode="system",instance="localhost:9100"}[1m])
~~~

##### 2.5 expose custom metrics VIA python client library, Textfile Collector

Use Python to expose custom metrics (some setup involved). 

relevant components:
[Node Exporter Textfile collector](https://github.com/prometheus/node_exporter#textfile-collector)

To use the textfile collector, set the `--collector.textfile.directory` flag on the `node_exporter` command line. The collector will parse all files in that directory matching the glob `*.prom`  

Best option for now is to use the `python` client library, [found here](https://github.com/prometheus/client_python) to export custom metrics to whatever textfile directory was set in the above example.  

##### 3. Use the Python client library to expose custom metrics

Make sure you have pip installed, you can check by running the command   `pip --version`

Run `pip install prometheus_client`   

This python module will allow you to store metrics according to metric types, which you can view [here](https://prometheus.io/docs/concepts/metric_types/)

---
An example python script for collecting custom metrics into the textfile collector functionality can be viewed below:

~~~python
from prometheus_client import CollectorRegistry, Gauge, write_to_textfile

registry = CollectorRegistry()
g = Gauge('osrs_cpu', 'how much compute oldschool rs is using', registry=registry)


#g.set(data)
#write_to_textfile('/home/aaron/osrs.prom', registry)
~~~
