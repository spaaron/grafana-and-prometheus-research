### Basics for configuring Grafana & Prometheus

[monitoring_setup.md](./monitoring_setup.md) --
guide for installing & configuring Prometheus, Grafana, and Pushgateway.   

[node_exporter.md](./node_exporter.md) -- Guide to help configure node_exporter, and some notes about custom metrics.

