### Components:
+ Bash script - for gathering localhost cpu metrics
+ prometheus - time series database to store metrics.  Scrapes pushgateway (below) to fetch & store metrics.
+ pushgateway - A prometheus component to act as a cache to store data from bash script.
+ Grafana - Dashboard monitoring tool that retrieves data from prometheus VIA promQL and plots it.

<!--prometheus traditionally scrapes metrics from http instances. The bash script offers no http instances for prometheus to scrape. This is why we use pushgateway. -->

#### Setup:
+ Go to [prometheus.io/download/](prometheus download page) and download prometheus, and pushgateway.

Then,

~~~cmd
~$ tar xvzf pushgateway-*
~$ tar xvzf prometheus-*
~$ vi prometheus.yml
~~~

+ cd into the newly created prometheus directory to access prometheus.yml, which you can use to adjust your scrape interval, add ports to listen to, etc.

~~~yml
#adjust how often we scrape data
scrape_interval: 15s
evaluation_interval: 15s
#add port 9091, pushgateway's default port
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: "prometheus"

    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.

    static_configs:
      - targets: ["localhost:9090", "localhost:9091"]
~~~

+ Install Grafana

Visit the installation guide for debian, or whatever system you run.  Always understand commands before you run them!

The following information was found on Grafana's install guide (Sept 2021):
[https://grafana.com/docs/grafana/latest/installation/debian/](Grafana - Debian Installation Guide)

Steps we followed:
+ add the grafana gpg key
+ add the source repositories for the grafana APT package
+ installed the grafana-enterprise (perhaps choose OSS version instead, depending on your needs)
+ Configure grafana to run using systemd

~~~sh
~$ sudo systemctl daemon-reload
~$ sudo systemctl start grafana-server
~$ sudo systemctl status grafana-server
~~~
+ configure the grafana-server service to start at boot
~~~sh
~$ sudo systemctl enable grafana-server.service 
~~~

Then navigate to the "getting started " page for grafana, and follow instructions to login on [localhost:3000](grafana local instance), the default homepage.

we changed the http method from POST to GET in grafana's prometheus settings


### create a bash script to obtain desired metrics:


### CHECKPOINT:
at this point, we did not create any jobs that automatically start up with the system.  In order to boot up the stack in this current state,
do the following:

~~~sh
cd ~/Downloads
cd prometheus-*
./prometheus
cd ../pushgateway-*
./pushgateway &
cd ~/
#then run
while sleep 1; do ./get-cpu-data.sh; done;
~~~
